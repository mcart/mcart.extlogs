<?php
IncludeModuleLangFile(__FILE__);

class mcart_extlogs extends CModule
{
	var $MODULE_ID = "mcart.extlogs";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	//var $MODULE_GROUP_RIGHTS = "Y";
    var $PARTNER_NAME;
    var $PARTNER_URI;

	function mcart_extlogs()
	{
        $this->PARTNER_NAME = GetMessage('PARTNER_NAME');
        $this->PARTNER_URI = "http://mcart.ru/";
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}

		$this->MODULE_NAME = GetMessage("MC_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("MC_MODULE_DESCRIPTION");
	}

	function DoInstall()
	{
		$this->InstallFiles();
		$this->InstallDB();
	}

	function InstallDB()
	{

		RegisterModule("mcart.extlogs");

		$eventManager = \Bitrix\Main\EventManager::getInstance();

		$eventManager->registerEventHandler('iblock', 'OnBeforeIBlockSectionAdd', 'mcart.extlogs', 'CExtLog', 'OnBeforeIBlockSectionAddHandler');
        $eventManager->registerEventHandler('iblock', 'OnBeforeIBlockSectionUpdate', 'mcart.extlogs', 'CExtLog', 'OnBeforeIBlockSectionUpdateHandler');
        $eventManager->registerEventHandler('main', 'OnBeforeUserUpdate', 'mcart.extlogs', 'CExtLog', 'OnBeforeUserUpdateHandler');
        //$eventManager->registerEventHandler('ldap', 'OnLdapUserFields', 'mcart.extlogs', 'CExtLog', 'OnLdapUserFields');

		return true;
	}

	function InstallFiles()
	{

		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function DoUninstall()
	{

			$this->UnInstallDB();
			$this->UnInstallFiles();
	}

	function UnInstallDB()
	{
		$eventManager = \Bitrix\Main\EventManager::getInstance();
		$eventManager->unRegisterEventHandler('iblock', 'OnBeforeIBlockSectionAdd', 'mcart.extlogs', 'CExtLog', 'OnBeforeIBlockSectionAddHandler');
        $eventManager->unRegisterEventHandler('iblock', 'OnBeforeIBlockSectionUpdate', 'mcart.extlogs', 'CExtLog', 'OnBeforeIBlockSectionUpdateHandler');
        $eventManager->unRegisterEventHandler('main', 'OnBeforeUserUpdate', 'mcart.extlogs', 'CExtLog', 'OnBeforeUserUpdateHandler');
        //$eventManager->unRegisterEventHandler('ldap', 'OnLdapUserFields', 'mcart.extlogs', 'CExtLog', 'OnLdapUserFields');
		UnRegisterModule("mcart.extlogs");

		return true;
	}

	function UnInstallFiles()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}
}
