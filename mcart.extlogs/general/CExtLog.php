<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//use Bitrix\Main\Type;

class CExtLog
{
    function OnBeforeIBlockSectionAddHandler(&$arFields)
    {
        /*echo "<pre>";
        print_r($arFields);
        echo "</pre>";
        die;*/
        $keyChange = array();
        if($arFields['IBLOCK_ID'] == 5) {
            foreach ($arFields as $key => $field) {
                if(!empty($field)){
                    $keyChange[] = $key." = ".$field;
                }
            }

            CEventLog::Add(array(
                "SEVERITY" => "SECURITY",
                "AUDIT_TYPE_ID" => "Information block section changed",
                "ITEM_ID" => $arFields['ID'],
                "MODULE_ID" => "mcart.expstructextlogs",
                "DESCRIPTION" => "changed:"." ".implode(", ", $keyChange)
            ));
        }
    }

    function OnBeforeIBlockSectionUpdateHandler(&$arFields)
    {

        if($arFields['IBLOCK_ID'] == 5) {
            $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), array('IBLOCK_ID' => $arFields['IBLOCK_ID'], 'ID' => $arFields['ID']), false, array("UF_*"));
            $section = array();
            while ($arSect = $rsSect->GetNext()) {
                $section = $arSect;
                //echo "<pre>";
                //print_r($arSect);
                //echo "</pre>";
            }
            //unset($arFields['UF_TM_REPORT_TPL']);
            unset($arFields['IPROPERTY_TEMPLATES']);
            //echo "<pre>";
            //print_r($arFields);
            //echo "</pre>";
            //die;
            $keyChange = array();
            foreach ($arFields as $key => $field) {

                if(is_array($field)){
                    foreach ($field as $key2 => $item){
                        if(empty($item)){
                            unset($field[$key2]);
                        }
                    }
                }

                if (isset($section[$key])) {
                    if ($section[$key] != $field) {
                        if(is_array($field)) {
                            $keyChange[] = $key . " = " . serialize($field);
                        } else {
                            $keyChange[] = $key . ' = "' . $field.'"';
                        }
                    }
                } else {
                    if(!empty($field)) {
                        if(is_array($field)) {
                            $keyChange[] = $key." = ".serialize($field);
                        } else {
                            $keyChange[] = $key . ' = "' . $field.'"';
                        }
                    }
                }
            }
            /*echo "<pre>";
            print_r($section);
            echo "</pre>";
            echo "<pre>";
            print_r($arFields);
            echo "</pre>";
            die;*/

            if (count($keyChange) > 0) {
                CEventLog::Add(array(
                    "SEVERITY" => "SECURITY",
                    "AUDIT_TYPE_ID" => "Information block section changed",
                    "ITEM_ID" => $arFields['ID'],
                    "MODULE_ID" => "mcart.expstructextlogs",
                    "DESCRIPTION" => "changed:"." ".implode(", ", $keyChange)
                ));
            }
        }

    }

    function OnBeforeUserUpdateHandler(&$arFields)
    {
        if (empty($arFields["DN"])) {
            $rsUser = CUser::GetByID($arFields['ID']);
            $arUser = $rsUser->Fetch();

            if ($arUser['ACTIVE'] == "N") {
                $arUser['ACTIVE'] = "";
            }
            if ($arUser['PERSONAL_COUNTRY'] == 0) {
                $arUser['PERSONAL_COUNTRY'] = "";
            }

            if ($arUser['WORK_COUNTRY'] == 0) {
                $arUser['WORK_COUNTRY'] = "";
            }

            $keyChange = array();
            foreach ($arFields as $key => $field) {

                if ($key == "GROUP_ID") {
                    /*$arGroups = CUser::GetUserGroup($arFields["ID"]);
                    foreach ($field as $fi) {
                        echo $fi["GROUP_ID"];
                        if (in_array($fi["GROUP_ID"], $arGroups)) {
                            $flag = true;
                        } else {
                            $flag = false;
                            break;
                        }
                    }

                    if($flag == true){
                        continue;
                    }*/
                    continue;

                }

                if (is_array($field)) {
                    foreach ($field as $key2 => $item) {
                        if (empty($item)) {
                            unset($field[$key2]);
                        }
                    }
                }

                if (isset($arUser[$key])) {
                    if ($arUser[$key] != $field) {
                        if (is_array($field)) {
                            $keyChange[] = $key . " = " . serialize($field);
                        } else {
                            $keyChange[] = $key . ' = "' . $field . '"';
                        }
                    }
                } else {
                    if (!empty($field)) {
                        if (is_array($field)) {
                            $keyChange[] = $key . " = " . serialize($field);
                        } else {
                            $keyChange[] = $key . ' = "' . $field . '"';
                        }
                    }
                }
            }
            /*echo "<pre>";
            print_r($arGroups);
            echo "</pre>";
            echo "<pre>";
            print_r($arFields);
            echo "</pre>";
            die;*/
            if (count($keyChange) > 0) {
                CEventLog::Add(array(
                    "SEVERITY" => "SECURITY",
                    "AUDIT_TYPE_ID" => "Edit user",
                    "ITEM_ID" => $arFields['ID'],
                    "MODULE_ID" => "mcart.expstructextlogs",
                    "DESCRIPTION" => "changed:" . " " . implode(", ", $keyChange)
                ));
            }
            /*echo "<pre>";
            print_r($arUser);
            echo "</pre>";
            echo "<pre>";
            print_r($arFields);
            echo "</pre>";
            die;*/

        }
    }

}